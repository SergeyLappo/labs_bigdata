package com.sergeylappo.lab3

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForObject
import java.net.URI

@RestController
@RequestMapping(value = ["/gateway"])
class GatewayController {
    @Autowired
    lateinit var restTemplate: RestTemplate

    @Autowired
    lateinit var env: Environment

    @RequestMapping(value = ["/data"], method = [RequestMethod.GET])
    fun getData(): String {
        println("Returning data from gateway");
        return "Hello from gateway";
    }

    @RequestMapping(value = ["/server-data"], method = [RequestMethod.GET])
    fun getServerData(): String {
        println("Returning data from serer through gateway");
        try {
            val msEndpoint = env.getProperty("endpoint.server");
            println("Endpoint name : [$msEndpoint]");
            return restTemplate.getForObject(URI(msEndpoint!!))
        } catch (ex: Exception) {
            ex.printStackTrace();
        }
        return "Exception occurred";
    }
}