package com.sergeylappo.lab3

import org.apache.http.conn.ssl.NoopHostnameVerifier
import org.apache.http.conn.ssl.SSLConnectionSocketFactory
import org.apache.http.conn.ssl.TrustSelfSignedStrategy
import org.apache.http.impl.client.HttpClients
import org.apache.http.ssl.SSLContextBuilder
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.core.io.ClassPathResource
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate
import java.io.InputStream
import java.security.KeyStore

@SpringBootApplication
class Lab3Application {
    @Bean
    fun restTemplate(): RestTemplate {
        val restTemplate = RestTemplate()
        val keyStore: KeyStore
        val requestFactory: HttpComponentsClientHttpRequestFactory?
        try {
            keyStore = KeyStore.getInstance("jks")
            val classPathResource = ClassPathResource("gateway.jks")
            val inputStream: InputStream = classPathResource.inputStream
            keyStore.load(inputStream, "123456".toCharArray())
            val socketFactory = SSLConnectionSocketFactory(SSLContextBuilder()
                    .loadTrustMaterial(null, TrustSelfSignedStrategy())
                    .loadKeyMaterial(keyStore, "123456".toCharArray()).build(),
                    NoopHostnameVerifier.INSTANCE)
            val httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory)
                    .setMaxConnTotal(5)
                    .setMaxConnPerRoute(5)
                    .build()
            requestFactory = HttpComponentsClientHttpRequestFactory(httpClient)
            requestFactory.setReadTimeout(10000)
            requestFactory.setConnectTimeout(10000)
            restTemplate.requestFactory = requestFactory
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
        return restTemplate
    }

}

fun main(args: Array<String>) {
    runApplication<Lab3Application>(*args)
}

