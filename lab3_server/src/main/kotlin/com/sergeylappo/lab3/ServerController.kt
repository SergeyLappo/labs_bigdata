package com.sergeylappo.lab3

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping(value = ["/server"])
class ServerController {
    @RequestMapping(value = ["/data"], method = [RequestMethod.GET])
    fun getData(): String {
        println("Returning data from server")
        return "Hello from server"
    }
}