import com.sun.org.apache.xml.internal.serialize.OutputFormat
import com.sun.org.apache.xml.internal.serialize.XMLSerializer
import org.w3c.dom.Document
import org.w3c.dom.Node
import org.xml.sax.InputSource
import java.io.File
import java.io.FileOutputStream
import java.io.PrintStream
import java.io.StringReader
import javax.xml.parsers.DocumentBuilderFactory


private const val source = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz"
private const val target = "Q5A8ZWS0XEDC6RFVT9GBY4HNU3J2MI1KO7LPqazwsxedcrfvtgbyhnujmikolp"

enum class TransformationType {
    OBFUSCATE,
    DEOBFUSCATE
}

fun transform(s: String, type: TransformationType): String {
    return when (type) {
        TransformationType.OBFUSCATE -> transform(s, source, target)
        TransformationType.DEOBFUSCATE -> transform(s, target, source)
    }
}

fun transform(s: String, source: String, target: String): String {
    val result = StringBuilder(s.length)
    for (i in s) {
        val index: Int = source.indexOf(i)
        result.append(target[index])
    }
    return result.toString()
}

fun main(args: Array<String>) {
    val file = readXml(args[1])
    when {
        args.size != 3 -> throw IllegalArgumentException("Expected one argument")
        args[0].equals("obfuscate", ignoreCase = true) -> {
            val newFile = FileOutputStream(args[2], false)
            System.setOut(PrintStream(newFile))
            for (i in (0 until file.childNodes.length)) {
                obfuscate(file, file.childNodes.item(0), TransformationType.OBFUSCATE)
            }
            val format = OutputFormat(file)
            format.indenting = true
            val serializer = XMLSerializer(System.out, format)
            serializer.serialize(file)
        }
        args[0].equals("deobfuscate", ignoreCase = true) -> {
            val newFile = FileOutputStream(args[2], false)
            System.setOut(PrintStream(newFile))
            for (i in (0 until file.childNodes.length)) {
                obfuscate(file, file.childNodes.item(i), TransformationType.DEOBFUSCATE)
            }
            val format = OutputFormat(file)
            format.indenting = true
            val serializer = XMLSerializer(System.out, format)
            serializer.serialize(file)
        }
        else -> throw IllegalArgumentException("Unknown run type")
    }
}

private fun obfuscate(document: Document, node: Node, type: TransformationType) {
    for (child in (0 until node.childNodes.length)) {
        obfuscate(document, node.childNodes.item(child), type)
    }
    if (node.textContent.trim().isEmpty()) return
    if (node.nodeValue?.isEmpty() != false) {
        for (attrib in (0 until node.attributes.length)) {
            node.attributes.item(attrib).nodeValue = transform(node.attributes.item(attrib).nodeValue
                    ?: "", type)
            document.renameNode(node.attributes.item(attrib), transform(node.attributes.item(attrib).namespaceURI
                    ?: "", type),
                    transform(node.attributes.item(attrib).nodeName ?: "", type))
        }
        document.renameNode(node, transform(node.namespaceURI ?: "", type),
                transform(node.nodeName ?: "", type))
    } else {
        node.nodeValue = transform(node.nodeValue ?: "", type)
    }
}

fun readXml(path: String): Document {
    val xmlFile = File(path)

    if (!xmlFile.exists()) {
        throw IllegalStateException("File does not exists")
    }

    val dbFactory = DocumentBuilderFactory.newInstance()
    val dBuilder = dbFactory.newDocumentBuilder()
    val xmlInput = InputSource(StringReader(xmlFile.readText()))

    return dBuilder.parse(xmlInput)
}