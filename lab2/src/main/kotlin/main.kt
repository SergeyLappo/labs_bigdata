import org.bouncycastle.asn1.ASN1InputStream
import org.bouncycastle.asn1.cms.ContentInfo
import org.bouncycastle.cert.X509CertificateHolder
import org.bouncycastle.cert.jcajce.JcaCertStore
import org.bouncycastle.cms.*
import org.bouncycastle.cms.jcajce.*
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder
import org.bouncycastle.util.Selector
import java.io.ByteArrayInputStream
import java.io.FileInputStream
import java.security.KeyStore
import java.security.PrivateKey
import java.security.Security
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate


fun main() {
    Security.addProvider(BouncyCastleProvider())
    val certFactory = CertificateFactory.getInstance("X.509", "BC")
    val certificate = certFactory.generateCertificate(FileInputStream("src/main/resources/public.cer"))
            as X509Certificate

    val keystorePassword = "password".toCharArray()
    val keyPassword = "password".toCharArray()
    val keystore = KeyStore.getInstance("PKCS12")
    keystore.load(FileInputStream("src/main/resources/private.p12"), keystorePassword)
    val privateKey = keystore.getKey("baeldung", keyPassword) as PrivateKey


    val secretMessage = "My password is 123456Seven"
    println("Original Message : $secretMessage")
    val stringToEncrypt = secretMessage.toByteArray()
    val encryptedData = encryptData(stringToEncrypt, certificate)
    println("Encrypted Message : " + String(encryptedData))
    val rawData = decryptData(encryptedData, privateKey)
    val decryptedMessage = String(rawData)
    println("Decrypted Message : $decryptedMessage")

    val signedData = signData(rawData, certificate, privateKey)
    println("Data signed")
    val check = verifySignedData(signedData)
    println("Data is valid $check")
}


fun encryptData(data: ByteArray, encryptionCertificate: X509Certificate): ByteArray {
    val cmsEnvelopedDataGenerator = CMSEnvelopedDataGenerator()
    val jceKey = JceKeyTransRecipientInfoGenerator(encryptionCertificate)
    cmsEnvelopedDataGenerator.addRecipientInfoGenerator(jceKey)
    val msg = CMSProcessableByteArray(data)
    val encryptor = JceCMSContentEncryptorBuilder(CMSAlgorithm.AES256_CBC)
            .setProvider("BC").build()
    val cmsEnvelopedData = cmsEnvelopedDataGenerator.generate(msg, encryptor)
    return cmsEnvelopedData.encoded
}

fun decryptData(encryptedData: ByteArray, decryptionKey: PrivateKey): ByteArray {
    val envelopedData = CMSEnvelopedData(encryptedData)
    val recipients = envelopedData.recipientInfos.recipients
    val recipientInfo = recipients.iterator().next()
    val recipient = JceKeyTransEnvelopedRecipient(decryptionKey)
    return recipientInfo.getContent(recipient)
}

fun signData(data: ByteArray, signingCertificate: X509Certificate, signingKey: PrivateKey): ByteArray {
    val certList = ArrayList<X509Certificate>()
    val cmsData = CMSProcessableByteArray(data)
    certList.add(signingCertificate)
    val certs = JcaCertStore(certList)
    val cmsGenerator = CMSSignedDataGenerator()
    val contentSigner = JcaContentSignerBuilder("SHA256withRSA").build(signingKey)
    cmsGenerator.addSignerInfoGenerator(
            JcaSignerInfoGeneratorBuilder(JcaDigestCalculatorProviderBuilder().setProvider("BC").build())
                    .build(contentSigner, signingCertificate))
    cmsGenerator.addCertificates(certs)
    val cms = cmsGenerator.generate(cmsData, true)
    return cms.encoded
}

fun verifySignedData(signedData: ByteArray): Boolean {
    val inputStream = ByteArrayInputStream(signedData)
    val asnInputStream = ASN1InputStream(inputStream)
    val cmsSignedData = CMSSignedData(ContentInfo.getInstance(asnInputStream.readObject()))
    val signers = cmsSignedData.signerInfos
    val signer = signers.signers.iterator().next()
    val certCollection = cmsSignedData.certificates.getMatches(signer.sid as Selector<X509CertificateHolder>)
    val certHolder = certCollection.iterator().next()
    return signer.verify(JcaSimpleSignerInfoVerifierBuilder().build(certHolder));
}